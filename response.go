package centurion

import (
	"encoding/json"
	"net/http"
)

type responseContext struct {
	w http.ResponseWriter
	c *Context
}

func (c *Context) Response(w http.ResponseWriter) *responseContext {
	return &responseContext{w, c}
}

func (resp *responseContext) JSON(response interface{}) {
	resp.w.WriteHeader(200)
	resp.w.Header().Add("Content-Type", "text/json")
	json.NewEncoder(resp.w).Encode(response)
}
