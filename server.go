package centurion

import (
	"net/http"
	"strconv"

	"github.com/NYTimes/gziphandler"

	"golang.org/x/crypto/acme/autocert"
)

type Server struct {
	Router    http.Handler
	PortHTTP  int
	PortHTTPS int
}

func (s *Server) Start(c *Context) {
	defer c.Close()
	var err error
	s.setDefaults()
	s.Router = gziphandler.GzipHandler(s.Router)

	if s.PortHTTPS != 0 {
		m := autocert.Manager{
			Prompt: autocert.AcceptTOS,
			Cache:  autocert.DirCache(c.httpsCertificatesLocation),
		}
		go http.ListenAndServe(":"+strconv.Itoa(s.PortHTTP), m.HTTPHandler(nil))

		httpsServ := http.Server{
			Addr:      ":" + strconv.Itoa(s.PortHTTPS),
			TLSConfig: m.TLSConfig(),
			Handler:   s.Router,
		}

		err = httpsServ.ListenAndServeTLS("", "")

	} else {
		c.InfoLogger.Println("Starting server on port ", s.PortHTTP)
		err = http.ListenAndServe(":"+strconv.Itoa(s.PortHTTP), s.Router)
	}
	if err != nil {
		c.ErrLogger.Println(err)
	}
}

func (s *Server) setDefaults() {

	if s.PortHTTP == 0 {
		s.PortHTTP = 8000
	}
}

type logger interface {
	Println(...interface{})
}
