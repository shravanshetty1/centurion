package centurion

import "sync"

type dependencyContext struct {
	mutex        sync.RWMutex
	dependencies map[string]interface{}
}

func (c *Context) AddDependencies(dependencies map[string]interface{}) {
	c.InjectedDependencies = &dependencyContext{dependencies: dependencies}
}

func (d *dependencyContext) Get(key string) interface{} {
	if d == nil {
		return d
	}

	d.mutex.RLock()
	defer d.mutex.RUnlock()
	return d.dependencies[key]
}
