# Centurion - Personal minimalistic web framework

This is a minimalistic go framework designed around ease of use and extensibility


### Usage
Framework has 2 exported objects Server and Context. Context is injected into every request Server accepts.  
```

    context := centurion.New(pkg.BaseDir)

	router := mux.NewRouter()
	router.Use(middlewares.Authentication)
	router.NewRoute().Methods("GET").PathPrefix("/").Handler(context.Handle(website.Serve))

	server := &centurion.Server{
		Router:   router,
		PortHTTP: 8000,
		//PortHTTPS:                 8001,
	}
	server.Start(context)
```
