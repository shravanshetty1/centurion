package centurion

import (
	"encoding/json"
	"net/http"
)

type errorContext struct {
	w http.ResponseWriter
	c *Context
}

func (c *Context) Error(w http.ResponseWriter) *errorContext {
	return &errorContext{w, c}
}

func (e *errorContext) InvalidParam(param string) {
	e.w.WriteHeader(400)
	e.w.Header().Set("Content-Type", "text/json")
	json.NewEncoder(e.w).Encode(struct {
		Message string `json:"message"`
	}{
		Message: param + " is not valid",
	})
}

func (e *errorContext) MissingParam(param string) {
	e.w.WriteHeader(400)
	e.w.Header().Set("Content-Type", "text/json")
	json.NewEncoder(e.w).Encode(struct {
		Message string `json:"message"`
	}{
		Message: param + " is missing",
	})
}

func (e *errorContext) Generic(message string) {
	e.c.ErrLogger.Println(message)

	e.w.WriteHeader(500)
	e.w.Header().Set("Content-Type", "text/json")
	json.NewEncoder(e.w).Encode(struct {
		Message string `json:"message"`
	}{
		Message: "Something unexpected has occurred",
	})
}

func (e *errorContext) Client(message string) {
	e.c.ErrLogger.Println(message)

	e.w.WriteHeader(400)
	e.w.Header().Set("Content-Type", "text/json")
	json.NewEncoder(e.w).Encode(struct {
		Message string `json:"message"`
	}{
		Message: message,
	})
}
