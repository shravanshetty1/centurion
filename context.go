package centurion

import (
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/dgraph-io/badger"
)

type Context struct {
	Badger                    *badger.DB
	ErrLogger                 logger
	InfoLogger                logger
	InjectedDependencies      *dependencyContext
	handler                   func(c *Context, w http.ResponseWriter, r *http.Request)
	httpsCertificatesLocation string
}

func New(baseDir string) *Context {
	db, err := badger.Open(badger.DefaultOptions(filepath.Join(baseDir, "badger")))
	if err != nil {
		panic(err)
	}

	errLog := log.New(os.Stderr, "[ERROR]", log.LstdFlags|log.Llongfile)
	infoLog := log.New(os.Stdout, "[ERROR]", log.LstdFlags|log.Llongfile)

	return &Context{
		httpsCertificatesLocation: filepath.Join(baseDir + "/tlsData"),
		Badger:                    db,
		ErrLogger:                 errLog,
		InfoLogger:                infoLog,
	}
}

func (c *Context) Close() {
	err := c.Badger.Close()
	if err != nil {
		c.ErrLogger.Println(err)
	}
}

func (c *Context) Handle(handler func(c *Context, w http.ResponseWriter, r *http.Request)) *Context {
	newContext := *c
	newContext.handler = handler
	return &newContext
}

func (c *Context) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	c.handler(c, w, r)
}
